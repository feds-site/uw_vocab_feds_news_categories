<?php

/**
 * @file
 * uw_vocab_feds_news_categories.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_vocab_feds_news_categories_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
  if ($module == "taxonomy_display" && $api == "taxonomy_display") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_strongarm_alter().
 */
function uw_vocab_feds_news_categories_strongarm_alter(&$data) {
  if (isset($data['context_status'])) {
    $data['context_status']->value['feds_news_taxonomy'] = TRUE; /* WAS: '' */
  }
}

/**
 * Implements hook_rdf_default_mappings().
 */
function uw_vocab_feds_news_categories_rdf_default_mappings() {
  $schemaorg = array();

  // Exported RDF mapping: feds_news_categories
  $schemaorg['taxonomy_term']['feds_news_categories'] = array(
    'rdftype' => array(
      0 => 'skos:Concept',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'rdfs:label',
        1 => 'skos:prefLabel',
      ),
    ),
    'description' => array(
      'predicates' => array(
        0 => 'skos:definition',
      ),
    ),
    'vid' => array(
      'predicates' => array(
        0 => 'skos:inScheme',
      ),
      'type' => 'rel',
    ),
    'parent' => array(
      'predicates' => array(
        0 => 'skos:broader',
      ),
      'type' => 'rel',
    ),
  );

  return $schemaorg;
}
