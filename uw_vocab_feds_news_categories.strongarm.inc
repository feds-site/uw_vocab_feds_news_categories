<?php

/**
 * @file
 * uw_vocab_feds_news_categories.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function uw_vocab_feds_news_categories_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'field_bundle_settings_taxonomy_term__feds_news_categories';
  $strongarm->value = array();
  $export['field_bundle_settings_taxonomy_term__feds_news_categories'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'metatag_enable_taxonomy_term__feds_news_categories';
  $strongarm->value = TRUE;
  $export['metatag_enable_taxonomy_term__feds_news_categories'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'tvi_vocab_feds_news_categories';
  $strongarm->value = array(
    'is_default' => FALSE,
    'type' => 'vocab',
    'xid' => 'feds_news_categories',
    'status' => 1,
    'view_name' => 'feds_news',
    'display' => 'feds_news_taxonomy_filter_page',
    'pass_arguments' => 1,
  );
  $export['tvi_vocab_feds_news_categories'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'xmlsitemap_settings_taxonomy_term_feds_news_categories';
  $strongarm->value = array(
    'entity' => 'taxonomy_term',
    'bundle' => 'news_categories',
    'status' => 0,
    'priority' => 0.5,
  );
  $export['xmlsitemap_settings_taxonomy_term_feds_news_categories'] = $strongarm;

  return $export;
}
